# Trabalho 1 Gabriel Araújo Costa

#include<stdio.h>
#include"calculadora.h"

int main()
{
    int n, m, opcao, resultado;//inserindo as variaveis// inserting the variables
    printf("***OPERACOES SOMA E PRODUTO***\n");//IMPRIMINDO TITULO//PRINTING TITLE
    printf("Informe a operacao...\n");//USUARIO INFORMA OPERACAO COM 1(SOMA) 2(PRODUTO)//USER INFORM OPERATION WITH 1 (SUM) 2 (PRODUCT)
    printf("1.Soma\n");//OP SOMA// OP SUM
    printf("2.Produto\n");//OP PROD 
    scanf("%d",&opcao);//IDENTIFIC OPCAO DO USUARIO// IDENTIFIC. USER OPTION
    
    if((opcao!=1)&&(opcao!=2)){//IDENTIFIC SE A OPCAO TA CORRETA // IDENTIFIES IF THE OPTION IS CORRECT
        printf("Opcao invalida...\n");//CASO ESTEJA ERRADO // IF IT IS WRONG
        return 0;//FECHA PROG SE ESTIVER ERRADO // CLOSE PROG IF IT IS WRONG
    }
	//SE COLOCOU A OPCAO CORRETA ABRE AQUI // IF THE CORRECT OPTION IS SET OPEN HERE
    printf("Informe n\n");//inserir prim variavel n // insert variable n
    scanf("%d",&n);//identif variavel/variable n
    printf("Informe m\n");//inserir(insert) seg variavel/variable m
    scanf("%d",&m);//identif variavel/variable m
    
    if(opcao==1){//se mandou somar// if you added
        resultado = somar(n,m);//func criada .h para somar // function created to add
        printf("A soma dos numeros eh %d\n",resultado);// mostra resultado // shows result
    }
    else if(opcao==2){//opc 2 de produto // 2 option, product
        resultado = multiplicar(n,m);
        printf("O produto dos numeros eh %d\n",resultado);
    }
    return 0;
}